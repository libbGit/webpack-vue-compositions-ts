### 描述

此模板用户快速的创建项目，采用webpack+vue3+ typescript语法，提供两种模板语法，

1、setup方式 

2、defineComponent方式 


### 常见错误
1、不要对组件起名为 index.vue， 这样在vue-router中引用时
```
component: () => import( /* webpackChunkName: "roster" */ `_pages/views/roster/index.vue` ),
会报错TypeError: Failed to fetch dynamically imported module

可以用大驼峰法起名，比如 Index， PersonIndex。
```
