import axios from "@/services/axios";

export function fetchLogout() {
  return axios.get("/user/logout");
}

export function fetchUserInfo() {
  return axios.get("/service/user/info");
}
