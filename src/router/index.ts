import { createRouter, createWebHistory } from "vue-router";
import type { Router, RouteRecordRaw } from "vue-router";

import store from "../store";

import userRoute from "./user";

const routes: RouteRecordRaw[] = [...userRoute];

const router: Router = createRouter({
  history: createWebHistory("/" + process.env.VUE_APP_PROJECT_NAME),
  routes,
});

// 解决相同路径跳转报错
const VueRouterPush = router.push;
router.push = function push(to) {
  return VueRouterPush.call(this, to).catch((err) => err);
};

router.beforeEach(async (to, from, next) => {
  document.title = to.meta.title as string;
  next();
});

export default router;
