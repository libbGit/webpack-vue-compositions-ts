import type { RouteLocationNormalized, RouteRecordRaw } from "vue-router";
const routes: RouteRecordRaw[] = [
  {
    path: "",
    redirect: "/home",
  },
  {
    path: "/home",

    component: () =>
      import(
        /* webpackChunkName: "layout" */ "@/layouts/Index.vue"
      ),
    children: [
      {
        path: "",
        redirect: (to: RouteLocationNormalized) => {
          return {
            path: to.path + "/index",
          };
        },
      },
      {
        path: "index",
        meta: {
          title: "劳务系统",
        },
        component: () =>
          import(/* webpackChunkName: "clock" */ `_pages/home/Index.vue`),
      },
    ],
  },
];

export default routes;
