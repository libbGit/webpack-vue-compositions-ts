import { createStore } from "vuex";
import type { Store } from "vuex";
import VuexPersistence from "vuex-persist";

import user from "./user";
import common from "./common";

const modules = {
  user,
  common,
};

const store: Store<any> = createStore({
  state: {},
  mutations: {},
  actions: {},
  modules,
  plugins: [
    //由于 vuex-persistedstate 已经被废弃，所以转用vuex-persist
    new VuexPersistence({
      key: "vue store",
      storage: window.sessionStorage,
    }).plugin,
  ],
  strict: process.env.MODE == "development", //在生产环境启用导致性能损失
});

export default store;

