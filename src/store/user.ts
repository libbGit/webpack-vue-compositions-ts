import type { ActionContext } from "vuex";

type IState = {
  auth: string;
  userInfo: any;
};

const state: IState = {
  auth: "", //
  userInfo: {},
};

const mutations = {
  /**
   * 更新 state
   */
  UPDATE_AUTH(state: IState, value: any) {
    state.auth = value;
  },

  UPDATE_USERINFO(state: IState, data: any) {
    state.userInfo = { ...state.userInfo, ...data };
  },

  //重置state
  RESET_STATE(state: IState, value: any) {
    state.auth = "";
  },
};
const actions = {
  //重置state
  restState({ commit, dispatch }: ActionContext<any, any>) {
    commit("RESET_STATE");
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
