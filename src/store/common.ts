import type { ActionContext } from "vuex";
import _ from "lodash-es";

type IState = {
  showTags: boolean;
  menuTags: any[];
  selectProject: any;
  allDicts: any[];
};

const state: IState = {
  showTags: false,
  menuTags: [],
  selectProject: {}, //选中额项目信息
  allDicts: [], //所有字典值
};

const mutations = {
  //重置state
  RESET_STATE(state: IState, value: any) {
    state.showTags = false;
    state.menuTags = [];
  },

  UPDATE_SELECT_PROJECT(state: IState, value: any) {
    state.selectProject = _.cloneDeep(value);
  },

  UPDATE_ALLDICTS(state: IState, value: any) {
    state.allDicts = _.cloneDeep(value);
  },
};
const actions = {
  //重置state
  restState({ commit, dispatch }: ActionContext<any, any>) {
    commit("RESET_STATE");
  },
};
export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
