import type { App } from 'vue';

import IconFont from "./IconFont.vue";

//作为插件必须定义公开的 install 方法
export default {
  install: (app: App, options: any) => {
    //注册全局组件
    app.component(IconFont.name, IconFont);
  },
};
