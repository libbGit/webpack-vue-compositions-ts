//此处定义模块，比如引入的vue文件，其他 npm包
declare module "*.vue" {
  import type { DefineComponent } from "vue";
  const component: DefineComponent<{}, {}, any>;
  export default component;
}
