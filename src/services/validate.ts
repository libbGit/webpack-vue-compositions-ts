// 身份证校验,表单校验直接使用，函数调用直接validateIdcard(id)返回status为1则校验正确
export function validateIdcard(...params: any[]) {
  let id, callback, isForm;
  // 说明是表单校验
  if (params[0].validator) {
    id = params[1];
    callback = params[2];
    isForm = true;
  } else {
    // 是函数调用
    id = params[0];
    callback = (msg: string) => msg;
    isForm = false;
  }
  // 1 "验证通过!", 0 //校验不通过
  let format =
    /^(([1][1-5])|([2][1-3])|([3][1-7])|([4][1-6])|([5][0-4])|([6][1-5])|([7][1])|([8][1-2]))\d{4}(([1][9]\d{2})|([2]\d{3}))(([0][1-9])|([1][0-2]))(([0][1-9])|([1-2][0-9])|([3][0-1]))\d{3}[0-9xX]$/;
  //号码规则校验
  if (!format.test(id)) {
    if (isForm) {
      // 用于表单校验
      callback(new Error("身份证号码不合规"));
      return false;
    } else {
      // 用于函数校验
      return { status: 0, msg: "身份证号码不合规" };
    }
  }
  //区位码校验
  //出生年月日校验   前正则限制起始年份为1900;
  let year = id.substr(6, 4), //身份证年
    month = id.substr(10, 2), //身份证月
    date = id.substr(12, 2), //身份证日
    time = Date.parse(month + "-" + date + "-" + year), //身份证日期时间戳date
    now_time = new Date().getTime(), //当前时间戳
    dates = new Date(year, month, 0).getDate(); //身份证当月天数
  if (time > now_time || date > dates) {
    if (isForm) {
      // 用于表单校验
      callback(new Error("出生日期不合规"));
      return false;
    } else {
      // 用于函数校验
      return { status: 0, msg: "出生日期不合规" };
    }
  }
  //校验码判断
  let c = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]; //系数
  let b = ["1", "0", "X", "9", "8", "7", "6", "5", "4", "3", "2"]; //校验码对照表
  let id_array = id.split("");
  let sum = 0;
  for (let k = 0; k < 17; k++) {
    sum += parseInt(id_array[k]) * parseInt(String(c[k]));
  }
  if (id_array[17].toUpperCase() != b[sum % 11].toUpperCase()) {
    if (isForm) {
      // 用于表单校验
      callback(new Error("身份证校验码不合规"));
      return false;
    } else {
      // 用于函数校验
      return { status: 0, msg: "身份证校验码不合规" };
    }
  }
  if (isForm) {
    // 用于表单校验
    callback();
    return false;
  } else {
    // 用于函数校验
    return { status: 1, msg: "校验通过" };
  }
}

export function validatePhone(phone: string) {
  return /^1[3456789]\d{9}$/.test(phone);
}
