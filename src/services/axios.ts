import axios from "axios";
import QS from "qs";
import NProgress from "nprogress";
import { lazyMessage } from "@/services/use-util";
import router from "@/router";

const CancelToken = axios.CancelToken;

let paramsSerializer = function (params: any) {
  //a: ['b', 'c']  ==>'a[0]=b&a[1]=c'
  return QS.stringify(params, { arrayFormat: "indices" });
};

const instance = axios.create({
  baseURL:
  process.env.MODE == "development"
      ? process.env.VUE_APP_API_PREFIX
      : process.env.VUE_APP_API_HOST, //开发环境 /api, 线上环境直接 host地址
  timeout: 80000,
  withCredentials: true, //表示跨域请求时是否需要使用凭证，比如从192.168.100.199:12180到192.168.100.199:12196，则跨域
  paramsSerializer: function (params) {
    return paramsSerializer(params);
  },
  validateStatus: function (status) {
    return status >= 200 && status <= 500; // 默认的
  },
});

// instance.defaults.retry = 2;
// instance.defaults.retryDelay = 1000;

instance.interceptors.request.use((config) => {
  NProgress.start();

  return config;
});

//响应拦截器
instance.interceptors.response.use(
  (res) => {
    NProgress.done();

    const status = Number(res.status) || 200;
    let message = res.data.msg || res.data.message;
    let statusText = res.statusText;

    if (status !== 200) {
      if (status === 401) {
        lazyMessage.error(statusText, () => {});

        if (res.data.code == 2) {
          router.push({
            path: "/error-login",
          });
        } else if (res.data.code == 3) {
          router.push({
            path: "/no-limit",
          });
        } else if (res.data.code == 5) {
          router.push({
            path: "/not-enough-limit",
          });
        }

        return Promise.reject(new Error(message));
      } else {
        lazyMessage.error(statusText, () => {});
        return Promise.reject(new Error(statusText));
      }
    } else {
      if (typeof res.data == "string") {
        //logout就是 res.data 是个字符串
        return Promise.resolve(res.data);
      }

      if (res.data.code != 0) {
        lazyMessage.error(message, () => {});
        return Promise.reject(new Error(message));
      }
    }
    return Promise.resolve(res.data);
  },
  (error) => {
    NProgress.done();

    // SESSION失效，重定向到登录页
    if (error.response?.status == 510) {
      if (process.env.MODE == "production" || process.env.MODE == "test") {
        window.location.href = process.env.VUE_APP_SSO_URL || "";
      } else {
        lazyMessage.error("权限不够，可能是 session过期", () => {});
      }
      return;
    }

    return Promise.reject(new Error(error));
  }
);

export default instance;
