import axios from "./axios";
import { useStore } from "vuex";
import { computed } from "vue";
import _ from "lodash-es";
import mitt from "mitt";
import { BigNumber } from "bignumber.js";
import { Toast } from "vant";
import { encode, decode, Base64 } from "js-base64";
import type { Ref, ComputedRef } from "vue";

/**
 * 重写toFixed方法，因为toFixed是四舍六入五成双，
 * “五成双”
 * 当5后有数时，舍5进1；
 * 当5后⽆有效数字时，需要分两种情况来讲：
 *  ①5前为奇数，舍5不进；
 *  ②5前为偶数，舍5进1。（0是偶数）
 * @param {*} d 保留位数
 */
Number.prototype.toFixed = function (d: number) {
  return String(Math.round((this as number) * 10 ** d) / 10 ** d);
};
//加
Number.prototype.plus = function (d: number) {
  return new BigNumber(this as number).plus(d).toNumber();
};
//减
Number.prototype.minus = function (d: number) {
  return new BigNumber(this as number).minus(d).toNumber();
};
//乘
Number.prototype.multiply = function (d: number) {
  return new BigNumber(this as number).multipliedBy(d).toNumber();
};
//除
Number.prototype.divide = function (d: number) {
  return new BigNumber(this as number).dividedBy(d).toNumber();
};

/**
 * 文件下载
 * @param {*} data  下载的文件路径和文件名
 * @returns
 */
const downloadFile = function (data: any) {
  let url = data.path || data.url;
  const title = data.title; //如果这里带名称，则必须要是带有文件后缀的名称，如 123.xls,而不是123

  if (!url) {
    console.error("url不存在", data, url);
    Toast.fail("无法下载");
  } else {
    //download start
    try {
      url = window.decodeURI(url); //解码
      const link = document.createElement("a");
      const index = url.lastIndexOf("/");
      const name = url.substring(index + 1);
      link.download = title || name;
      link.style.display = "none";
      link.href = url;
      link.target = "_blank";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    } catch (error) {
      console.log("error===", error);
    }

    return true;
  }
};

// 下载二进制流文件
const downLoadBuffer = (options: any) => {
  axios({
    method: "get",
    url: options.url,
    responseType: "arraybuffer",
    params: options.params,
  }).then((res) => {
    // 假设 data 是返回来的二进制数据
    const data = res.data;
    const url = window.URL.createObjectURL(
      new Blob([data], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      })
    );
    const link = document.createElement("a");
    link.style.display = "none";
    link.href = url;
    link.setAttribute(
      "download",
      options.name ? `${options.name}.xlsx` : "excel.xlsx"
    );
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  });
};

/**
 * 判断是否为空数组或者空对象,null,undefined,
 */
const validateNull = (val: any) => {
  if (typeof val === "boolean") {
    return false;
  }
  if (typeof val === "number") {
    return false;
  }
  if (val instanceof Array) {
    if (val.length === 0) return true;
  } else if (val instanceof Object) {
    if (JSON.stringify(val) === "{}") return true;
  } else {
    return val == null || val === undefined || val === "";
    // 注释原来代码，原来代码中包含判断字符串'null'，字符串'undefined'
    // return val === 'null' || val == null || val === 'undefined' || val === undefined || val === ''
  }
  return false;
};

/**
 * 打开外部链接
 * @param {*} url
 */
const openExternalPage = function (url: string) {
  if (!url) {
    console.error("url不存在", url);
  } else {
    //download start
    try {
      let link = document.createElement("a");
      link.style.display = "none";
      link.href = url;
      link.target = "_blank";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    } catch (err) {
      console.log("error===", err);
    }
  }
};

/**
 * 用于上传文件的axios请求函数，用于 http-request 属性上
 * @param {*} params 参数
 * @returns
 */
const uploadHttpRequest = (params: any) => {
  //必须为 promise，这样才能执行完后，调用 on-success等钩子
  return new Promise((resolve, reject) => {
    let action = params.action; //上传的url

    let file = params.file; //上传的文件
    let data = params.data; //上传的附加 数据

    //开始上传
    let formData = new FormData();
    formData.set("file", file);
    formData.set("data", JSON.stringify(data));

    axios
      .post(action, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
        onUploadProgress: function (event) {
          let percent = (event.loaded / event.total) * 100;
          params?.onProgress?.({
            percent: Math.floor(percent * 100) / 100,
            total: event.total,
          });
        },
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(new Error(err.message));
      });
  });
};

/**
 * 自定义element  message，文本长时，展示时间也变长。且300ms内，只展示一条message
 */
const lazyMessage = {
  error: _.debounce((msg: string, onclose) => {
    let time = 2000 + msg?.length || 0 * 100;
    if (msg) {
      Toast.fail(msg);
    }
  }, 300),
  warning: _.debounce((msg: string, onclose) => {
    let time = 2000 + msg?.length || 0 * 100;
    if (msg) {
      Toast.fail(msg);
    }
  }, 300),

  success: _.debounce((msg: string, onclose) => {
    let time = 2000 + msg?.length || 0 * 100;
    if (msg) {
      Toast.success(msg);
    }
  }, 300),
  info: _.debounce((msg: string, onclose) => {
    let time = 2000 + msg?.length || 0 * 100;
    if (msg) {
      Toast(msg);
    }
  }, 300),
};

/**
 * 判断两个数组中的不同的项目    arr1=[1,2,3]  arr2=[2,3,4] 结果为 1,4
 * @param {*} arr1
 * @param {*} arr2
 * @returns
 */
const diffArray = (arr1: any[], arr2: any[]) => {
  return arr1.concat(arr2).filter(function (v, i, arr) {
    return arr.indexOf(v) === arr.lastIndexOf(v);
  });
};

const getQuery = (name: string) => {
  let search = location.search.replace("?", "");
  let searchArr = search.split("&");
  let obj: any = {};
  for (let item of searchArr) {
    let arr = item.split("=");
    obj[arr[0]] = arr[1];
  }
  return obj[name];
};

/**
 * 编码并加密  url中的query
 * @param {*} params
 * @returns
 */
const encryptPageId = (params: any): string => {
  let ciphertext = encode(JSON.stringify(params));
  let urlquery = encodeURIComponent(ciphertext); //将加密字符串中的 + ，=等符号编码，避免在url中变为空字符串
  return urlquery;
};

/**
 * 解密并解码  url中的query
 * @param {*} pageId
 * @returns
 */
const decryptPageId = (pageId: string): any => {
  try {
    let str = decodeURIComponent(pageId); //将url中的%2B，%3D等替换为+，=号。如果本来就包含+，=等没有变化
    let data = JSON.parse(decode(str));
    if (data) {
      return data;
    } else {
      return null;
    }
  } catch (error) {
    return null; //无法正确解码为一个json
  }
};

/**
 * eventHub.emit("reload-dashboard-list");
 * eventHub.on("reload-dashboard-list", (data) => {
        this.fetchAllTree();
   });
   eventHub.off("reload-dashboard-list");
 */
const eventHub = mitt();

//T用泛型
const useState = <T>(moduleName: string, stateName: string): ComputedRef<T> => {
  const store = useStore();
  return computed(() => store.state[moduleName][stateName]);
};
//T用泛型
const useGetters = <T>(
  moduleName: string,
  getterName: string
): ComputedRef<T> => {
  const store = useStore();
  return computed(() => store.getters[moduleName][getterName]);
};
const useMutation = (moduleName: string, mutationName: string) => {
  const store = useStore();
  return (value: any) => store.commit(`${moduleName}/${mutationName}`, value);
};
const useAction = (moduleName: string, actionName: string) => {
  const store = useStore();
  return (value: any) => store.dispatch(`${moduleName}/${actionName}`, value);
};

export {
  eventHub,
  downloadFile,
  downLoadBuffer,
  validateNull,
  uploadHttpRequest,
  lazyMessage,
  diffArray,
  openExternalPage,
  getQuery,
  useState,
  useGetters,
  useMutation,
  useAction,
  encryptPageId,
  decryptPageId,
};
