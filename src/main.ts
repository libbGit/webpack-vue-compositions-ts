import { createApp } from "vue";
import "@/assets/css/index.scss";
import "vant/lib/index.css";

import Vant from "vant";
import rootApp from "./App.vue";
import router from "./router";
import store from "./store";

import CommonComponent from "@/components";

import ElementPlus from "element-plus";

import "@/plugins/index.ts";
import "@/assets/font/index.js"; //引入多色 symbol方式的css

const application = createApp(rootApp);

application.use(Vant);
application.use(ElementPlus);
application.use(CommonComponent); //安装通用组件

application.use(store).use(router).mount("#app");
