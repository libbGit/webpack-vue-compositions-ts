import dayjs from "dayjs";
import "dayjs/locale/zh-cn"; //用来使 element-plus显示中文
// import updateLocale from 'dayjs/plugin/updateLocale';
import Weekday from "dayjs/plugin/weekday";

dayjs.extend(Weekday);

dayjs().weekday(-7); // element-plus不管任何语言 让星期一作为 一周的第一天
