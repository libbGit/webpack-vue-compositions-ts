import { defineConfig } from '@vue/cli-service';
import webpack from 'webpack';
import CompressionWebpackPlugin from 'compression-webpack-plugin'; //此模块版本必须和webpack相对应，既不能太高也不能太低，否则编译出错
import CleanUnusedFilesPlugin from 'clean-unusefile-webpack-plugin';
import Components from 'unplugin-vue-components/webpack';
import ElementPlusPlugin from 'unplugin-element-plus/webpack';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import { ESBuildMinifyPlugin } from 'esbuild-loader';
import { createRequire } from 'node:module';
import process from 'node:process';
const require = createRequire(import.meta.url);
let { name: packageName } = require('./package.json');

const API_HOST = process.env.VUE_APP_API_HOST;
const API_ENV = process.env.VUE_APP_ENV;
const PREFIX = process.env.VUE_APP_API_PREFIX;

/***************************配置开始****************************** */
export default defineConfig(() => {
  const config = {
    // 设置文件入口
    pages: {
      index: {
        entry: `src/main.ts`,
        // 模板来源
        template: 'public/index.html',
        // 在 dist/index.html 的输出
        filename: 'index.html',
        // 在这个页面中包含的块，默认情况下会包含
        // 提取出来的通用 chunk 和 vendor chunk。
        chunks: ['chunk-vendors', 'chunk-common', 'index'], //这里的 index 必须和入口名称 index 一致，然后使用  http://localhost:8135/index访问页面
        templateParameters: {
          //此处的参数可以在 index.html中直接用
          BASE_URL: '/',
        },
      },
    },
    outputDir: 'dist/' + packageName + '/',
    productionSourceMap: false, // 关闭生产环境的 source map
    lintOnSave: 'warning',
    publicPath: '/' + packageName,
    assetsDir: 'static',
    devServer: {
      compress: false,
      open: true, // 默认打开浏览器
      host: 'localhost',
      port: 8135,
      proxy: {
        [PREFIX]: {
          target: API_HOST,
          ws: true,
          changeOrigin: true,
          pathRewrite: {
            ['^' + PREFIX]: '',
          },
        },
      },
    },

    configureWebpack: {
      plugins: (function () {
        let plugins = [];
        //ProvidePlugin 自动加载模块，而不是起别名，别名需要import，而自动加载的模块不需要import既可以使用
        const pp = new webpack.ProvidePlugin({
          _: 'lodash-es',
          dayjs: 'dayjs',
          mathjs: 'mathjs',
          jQuery: 'jquery',
          $: 'jquery',
        });

        plugins.push(pp);

        if (API_ENV == 'production') {
          const cp = new CompressionWebpackPlugin({
            test: /\.(js|css|ts)?$/i, // 哪些文件要压缩
            filename: '[path][base].gz', // 压缩后的文件名
            algorithm: 'gzip', // 使用gzip压缩
            compressionOptions: {
              level: 9, //越大压缩越小，默认9
            },
            threshold: 10240, //大于 10240字节，既10k时
            minRatio: 0.9, //越大越小，默认 0.8
            deleteOriginalAssets: false, //是否删除原本的js。在开发环境需要关闭，在生产环境开启减少包的体积
          });

          plugins.push(cp);
        }

        const uf = new CleanUnusedFilesPlugin({
          root: './src', // 项目目录
          out: './unused-files.json', // 输出文件列表
          clean: false, // 删除文件,
          exclude: [''], // 排除文件列表, 格式为文件路径数组
        });

        plugins.push(uf);

        //按需加载
        plugins.push(
          Components({
            resolvers: [
              ElementPlusResolver({
                importStyle: 'sass',
              }),
            ],
          })
        );

        //自动在引入组件时，同时引入css
        plugins.push(
          ElementPlusPlugin({
            useSource: true,
          })
        );
        return plugins;
      })(),

      devtool: API_ENV == 'development' ? 'source-map' : false, //开发时开启

      resolve: {
        // 别名配置
        alias: {
          _common: '@/common',
          _pages: `@/pages`,
        },
      },
    },

    chainWebpack: (config) => {
      // 使用 esbuild 编译 js 文件
      const rule = config.module.rule('js');

      // 清理自带的 babel-loader
      rule.uses.clear();

      // 添加 esbuild-loader
      rule.use('esbuild-loader').loader('esbuild-loader').options({
        loader: 'jsx',
        target: 'es2015',
      });

      // 删除webpack底层默认的 terser, 换用 esbuild-minimize-plugin
      config.optimization.minimizers.delete('terser');

      // 使用 esbuild 优化 css 压缩
      config.optimization.minimizer('esbuild').use(ESBuildMinifyPlugin, [{ minify: true, css: true }]);

      config.plugin('define').tap((args) => {
        args[0]['process.env.VUE_APP_PROJECT_NAME'] = JSON.stringify(packageName);
        return args;
      });
    },

    css: {
      extract: API_ENV == 'production' ? true : false,
      sourceMap: API_ENV == 'production' ? true : false,
      loaderOptions: {
        scss: {
          additionalData: `@use "~/src/assets/css/element/element-override.scss" as *;`,
        },
      },
    },
  };

  return config;
});
